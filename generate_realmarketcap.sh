#!/bin/sh

# Market capitalisation or market cap is the market value of a company's issued share capital – in other words. the number of shares multiplied by the current price *of those shares on the stock market*. 
# http://lexicon.ft.com/Term?term=market-capitalisation

readonly LIQUIDITY_THRESHOLD=300000

readonly SOURCE='https://api.coinmarketcap.com/v1/ticker/?limit=0'
readonly DATA="`curl -s ${SOURCE} || curl -s --proxy socks5h://172.18.0.2:9050 ${SOURCE}`"
readonly ASSET_COUNT=`echo "${DATA}" | jq '.[].symbol' | wc -l`
#readonly ASSET_COUNT=3  # For debugging only

# Store a copy of the JSON
#readonly TICKER_FILE=ticker.json
#echo "${DATA}" > $TICKER_FILE

readonly TARGET="`date +%Y%m%d`"


generate_intro () {
        echo "h1. RealMarketCap - What's your favorite coin really worth?"
        echo
        echo "The total supply of any crypto currency is largely ignored by retail investors - and so is the fact that many coins or tokens are mostly still in the hands of their creators."
        echo
        echo "But what if all coins had an actual total supply equal to, let's say, Bitcoin's?"
        echo
        echo "Let's calculate the corresponding value ("corrected worth", second column in the table below) and see how it affects the ranking."
        echo
        echo 'With this metric applied, you can A) Find out how low the value of an asset actually is - or should be - and B) Easily spot assets that may potentially be overvalued.'
        echo
        echo 'NOTES:'
        echo
        echo "* We ignore any coin with low liquidity (less than USD ${LIQUIDITY_THRESHOLD} daily trading volume)."
        echo "* Only the top 100 assets according to CMC's sorting are considered."
        echo "* Since the data is sourced from CoinMarketCap, centralized tokens and centralized coins are included."
        echo
        echo "_Updated: `date --utc '+%Y/%m/%d %H:%M %Z'`_"
        echo
}

extend_ticker_data () {
        ticker=${DATA}

        btc_supply=`echo "${ticker}" | jq '.[] | select(.id == "bitcoin")' | jq -r '.total_supply'`

        i=0 ; while [ $i -lt $ASSET_COUNT ] ; do
                supply=`echo ${ticker} | jq -r ".[$i].available_supply"`

                if [ $supply != 'null' ] ; then
                        factor=`echo "scale=12; $supply / $btc_supply" | bc`
                        in_usd=`echo ${ticker} | jq -r ".[$i].price_usd"`
                        worth=`echo "scale=4 ; $in_usd / $factor" | bc | sed 's#^\.#0.#g'`
                        cheaphot_cap=`echo "scale=4 ; $in_usd * $btc_supply" | bc`

                        #factor_rounded=`echo "scale=4 ; $factor" | bc`
                        # add worth + factor + "cheap hot"-cap
                        ticker=`echo ${ticker} | jq ".[$i].worth=\"$worth\""`
                        ticker=`echo ${ticker} | jq ".[$i].factor=\"$factor\""`
                        ticker=`echo ${ticker} | jq ".[$i].cheaphot_cap=\"$cheaphot_cap\""`
                fi

                i=`expr $i + 1`
        done
        echo "${ticker}"
}

generate_table () {
        table=''
        r='s#\..*##g'
        # TODO do this directly with jq instead of looping
        i=0 ; while [ $i -lt $ASSET_COUNT ] ; do
                v=`echo ${ticker} | jq -r ".[$i].\"24h_volume_usd\"" | sed "$r"`

                if [ $v != 'null' -a $v -gt ${LIQUIDITY_THRESHOLD} ] ; then
                        c=`echo ${ticker} | jq -r ".[$i].cheaphot_cap" | sed "$r"`
                        f=`echo ${ticker} | jq -r ".[$i].factor"`
                        w=`echo ${ticker} | jq -r ".[$i].worth"`
                        n=`echo ${ticker} | jq -r ".[$i].name" | sed 's# #_#g'`
                        s=`echo ${ticker} | jq -r ".[$i].symbol"`
                        m=`echo ${ticker} | jq -r ".[$i].market_cap_usd" | sed "$r"`
                        s=`echo ${ticker} | jq -r ".[$i].available_supply" | sed "$r"`
                        p=`echo ${ticker} | jq -r ".[$i].price_usd"`
                        percent=`echo ${ticker} | jq -r ".[$i].percent_change_24h"`
                        
                        table="${table}
$w SORT_BY_THE_VARIABLE_TO_THE_LEFT_THEN_DELETE>.$w |>. ($p) | $n |>. $c |>. $m |>. $v |>. $s |>. (x $f) |>. $percent | "
                fi

                i=`expr $i + 1`
        done

        echo '|_. rank |_. corrected worth [USD] |_. (price) [USD] |_. name |_. "cheap-hot" cap |_. market cap |_. 24h volume |_. supply |_. (supply_factor) |_. 24h percent change | '
        rank=1
        echo "$table" | sed '/^\s*$/d' | sort -rn | while read line ; do 
                        echo "$line" | sed "s#^.*SORT_BY_THE_VARIABLE_TO_THE_LEFT_THEN_DELETE#|>. $rank |#g"
                        rank=`expr $rank + 1`
        done
        #table_sorted=`echo -e $table | sort -rn | sed 's#^#| #g'`
}

generate () {
        generate_intro

        ticker=`extend_ticker_data`
        #extend_ticker_data ${DATA}
        #echo $ticker | jq

        #remove_textile_alignments='s#<\.##g ; s#>\.##g'
        #generate_table | sed "${remove_textile_alignments}" | column  -R 2,3,5,6,7,8 -ts'|' -o'|'
        generate_table | column -ts'|' -o'|'
}

mkdir -p ./public && cd ./public && generate > ${TARGET}.textile
pandoc -f textile -t html5 --standalone --self-contained --metadata pagetitle="RealMarketCap on `date` | fullmetal.science" ${TARGET}.textile > ${TARGET}.html

rm index.html ; ln -s ${TARGET}.html index.html
